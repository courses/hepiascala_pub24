
trait Stack {
  def isEmpty: Boolean
  def pop: (Int,Stack)
}

def equals( stack1: Stack, stack2: Stack ): Boolean = {

  def loop( rest1: Stack, rest2: Stack ): Boolean = 
    if rest1.isEmpty && rest2.isEmpty then true
    else if ! rest1.isEmpty && ! rest2.isEmpty then
      val (x1,xs1) = rest1.pop
      val (x2,xs2) = rest2.pop
      if x1 == x2 then loop(xs1,xs2) else false
    else false

  //Autre correction possible 
  def loop_( rest1: Stack, rest2: Stack ): Boolean = (rest1.isEmpty, rest2.isEmpty) match {
    case (true,true) => true
    case (false,false) => 
      val (x1,xs1) = rest1.pop
      val (x2,xs2) = rest2.pop
      if x1 == x2 then loop_(xs1,xs2) else false
    case _ => false
  }
  loop(stack1, stack2)
}

import java.time.LocalDateTime

enum Status {
  case Pending
  case Done
}

enum Entry {
  case Action( description: String, pending: Status )
  case Note( description: String )
  case Event( descrition: String, date: LocalDateTime )
}

case class Entries(title: String, lastMod: LocalDateTime, entries: List[Entry] )

