def fac( n: Long ): Long =
  def fac_rec( n: Long, res: Long ): Long =
    if( n < 2 ) res
    else fac_rec( n-1, n*res )
  fac_rec(n,1)


def gcd( a: Int, b: Int ): Int =
  if b > a then gcd(b,a)
  else
    if b == 0 then 
      a
    else 
      gcd( b, a % b )

def syracuse1( n: Int ): Int =
  if n == 1 then 0
  else 
    val m = if n % 2 == 0 then n / 2
            else n*3 + 1
    1 + syracuse1(m)

def syracuse3( m: Int ): Int = 
  def loop( n: Int, i: Int ): Int =
    if n != 1 then
      if n % 2 == 0 then
        loop( n/2, i+1 )
      else
        loop( 3*n+1, i+1 )
    else
      i
  loop(m, 0)


/*
def dumbCount1( n: Int ): Int =
  if( n == 0 ) 0 else 1 + dumbCount1(n-1)

def dumbCount2( n: Int ): Int = {
  def dumby( m: Int, c: Int ): Int =
    if( m == 0) c else dumby(m-1,c+1)
  dumby( n, 0 )
}

  dumbCount(3)
  if( 3 == 0 ) 0 else 1 + dumbCount1(3-1)
  if( false ) 0 else 1 + dumbCount1(3-1)
  1 + dumbCount1(3-1)
  1 + dumbCount1(2)
  1 + (if( 2 == 0 ) 0 else 1 + dumbCount1(2-1))
  1 + (if( false ) 0 else 1 + dumbCount1(2-1))
  1 + (1 + dumbCount1(2-1))
  1 + (1 + dumbCount1(1))
  1 + (1 + (if( 1 == 0 ) 0 else 1 + dumbCount1(1-1)))
  1 + (1 + (if( false ) 0 else 1 + dumbCount1(1-1)))
  1 + (1 + (1 + dumbCount1(1-1)))
  1 + (1 + (1 + dumbCount1(0)))
  1 + (1 + (1 + (if( 0 == 0 ) 0 else 1 + dumbCount1(0-1))))
  1 + (1 + (1 + (if( true ) 0 else 1 + dumbCount1(0-1))))
  1 + (1 + (1 + 0))
  1 + (1 + 1)
  1 + 2
  3


dumby(3,0)
if( 3 == 0) 0 else dumby(3-1,0+1)
if( false ) 0 else dumby(3-1,0+1)
dumby(3-1,0+1)
dumby(2,0+1)
dumby(2,1)
if( 2 == 0) 1 else dumby(2-1,1+1)
if( false ) 1 else dumby(2-1,1+1)
dumby(2-1,1+1)
dumby(1,1+1)
dumby(1,2)
if( 1 == 0) 2 else dumby(1-1,2+1)
if( false ) 2 else dumby(1-1,2+1)
dumby(1-1,2+1)
dumby(0,2+1)
dumby(0,3)
if( 0 == 0 ) 3 else dumby(0-1,3+1)
if( true ) 3 else dumby(0-1,3+1)
3


def dumby( m: Int, c: Int ): Int =
    if( m == 0) c else dumby(m-1,c+1)

*/
