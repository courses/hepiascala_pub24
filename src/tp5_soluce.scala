enum Color(r: Byte, g: Byte, b: Byte) {
  case Red extends Color(-1, 0x00, 0x00)
  case Yellow extends Color(-1, -1, 0)
  case Green extends Color(0, -1, 0)
}

enum TrafficLight(c: Color) {
  case Stop extends TrafficLight(Color.Red)
  case Wait extends TrafficLight(Color.Yellow)
  case Go extends TrafficLight(Color.Green)
}

/*
 * Représente une pile d'entiers
 */
enum IntStack {
  case Elem(top: Int, rest: IntStack)
  case Empty

  /*
   * Retourne vrai ssi la pile est vide
   */
  def isEmpty = this match {
    case Elem(_, _) => false
    case Empty      => true
  }

  /*
   * Retourne la taille de la pile.
   * A IMPLEMNTER
   */
  def size: Int = this match {
    case Elem(_, r) => 1 + r.size
    case Empty      => 0
  }

  def size_ = {
    def loop(is: IntStack, n: Int): Int = is match {
      case Empty      => n
      case Elem(_, r) => loop(r, n + 1)
    }
    loop(this, 0)
  }

  /*
   * Ajoute un élément au sommet de la pile
   * A IMPLEMENTER
   */
  def push(i: Int): IntStack = Elem(i, this)

  /*
  val is1 = Elem(1, Elem(2, Empty ) )
  val (i,is2) = is1.pop
  i == 1
  is2 == Elem(2, Empty )
  val (j,is3) = is2.pop
  j == 2
  is3 == Empty
   */

  def addTop(is: IntStack): IntStack = {
    val (i, is2) = is.pop
    val (j, is3) = is2.pop
    is3.push(i + j)
  }

  /* addTop( Elem(10, Elem(20, Elem( 40, Empty ) ) ) )
    == Elem( 30, Elem( 40, Empty ) )
   */

  /*
   * Retire et retourne l'élément au sommet de la pile.
   * Si la pile est vide, cause une exception (moche).
   * A IMPLEMENTER
   */
  def pop: (Int, IntStack) = this match {
    case Elem(x, rest) => (x, rest)
    case Empty =>
      throw new NoSuchElementException("Empty stack")
  }

  /*
   * Prend l'élément du sommet de la pile et
   * le place en bas de la pile. Si la pile est
   * vide, ne fait rien.
   * A IMPLEMENTER
   */
  def rotate: IntStack = {
    val (i, rest) = this.pop
    rest.reverse.push(i).reverse
  }

  /*
   * Retourne l'élément tout en bas de la pile (sans le supprimer).
   * Si la pile est vide, lève une exception.
   * A IMPLEMENTER
   */
  def bottom: Int = this match {
    case Empty =>
      throw new NoSuchElementException("Empty stack")
    case Elem(i, Empty) => i
    case Elem(_, rest)  => rest.bottom
  }

  private def addRev(from: IntStack, to: IntStack): IntStack =
    from match {
      case Empty         => to
      case Elem(i, rest) => addRev(rest, to.push(i))
    }

  /*
   * Renverse des éléments de la pile. Si la pile est vide,
   * ne fait rien.
   * A IMPLEMENTER
   */
  def reverse: IntStack = addRev(this, Empty)

  /*
   * Place tous les éléments de la pile 'that' au sommet de la pile
   * actuelle.
   * A IMPLEMENTER
   */
  def pushAll(that: IntStack): IntStack =
    addRev(that.reverse, this)

}
