package ch.hepia.tp6_

enum Stack[+T] {
  case Elem( top: T, rest: Stack[T] )
  case Empty

  /*
   * Retourne vrai ssi la pile est vide
   */
  def isEmpty = this match {
    case Elem(_,_) => false
    case Empty => true
  }

  /*
   * Retourne la taille de la pile.
   */
    def size: Int = this match {
      case Elem(_,r) => 1 + r.size
      case Empty => 0
    }
   def sizeTR: Int = {
     def loop( s: Int, rest: Stack[T] ): Int = rest match {
       case Empty => s
       case Elem(_,r) => loop(s+1,r)
     }
     loop(0,this)
   }

  /*
   * Ajoute un élément au sommet de la pile
   */
    def push[U >: T]( i: U ): Stack[U] = Elem( i, this )

  /*
   * Retire et retourne l'élément au sommet de la pile.
   * Si la pile est vide, cause une exception (moche).
   */
    def pop: (T,Stack[T]) = this match {
      case Empty =>
        throw new NoSuchElementException("Empty stack")
      case Elem(v,r) => (v,r)
    }

    /*
     * Prend l'élément du sommet de la pile et
     * le place en bas de la pile. Si la pile est
     * vide, ne fait rien.
     */
    def rotate: Stack[T] = this match {
      case Empty => this
      case Elem( i, r ) => Empty.push(i).pushAll(r)
    }

    /*
     * Retourne l'élément tout en bas de la pile (sans le supprimer).
     * Si la pile est vide, lève une exception.
     */
    def bottom: T = this match {
      case Empty =>
        throw new NoSuchElementException("Empty stack")
      case Elem( i, Empty ) => i
      case Elem( _, r ) => r.bottom
    }
    def bottomTR: T = {
      def loop( rest: Stack[T] ): T = rest match {
        case Empty =>
         throw new NoSuchElementException("Empty stack")
        case Elem( i, Empty ) => i
        case Elem( _, r ) => loop(r)
      }
      loop(this)
    }
    /*
     * Renverse des éléments de la pile. Si la pile est vide,
     * ne fait rien.
     */
    //VERSION TR:
    def reverse: Stack[T] =
         pushAllRev( this, Empty )

    private def pushAllRev[V,W >: V]( cur: Stack[V], res: Stack[W] ): Stack[W] = cur match {
        case Empty => res
        case Elem(i,r) => pushAllRev( r, res.push(i) )
    }

    /*
     * Place tous les éléments de la pile 'that' au sommet de la pile
     * actuelle.
     */
    def pushAll[U >: T]( that: Stack[U] ): Stack[U] =
      pushAllRev( that.reverse:Stack[U], this )


    /*
     * Retourne une pile modifiée par l'appel d'une fonction
     * sur chaque élément
     */
    def map[U]( f: T=>U ): Stack[U] = {
      @scala.annotation.tailrec
      def loop(rest: Stack[T], result: Stack[U] ): Stack[U] = rest match
        case Empty => result.reverse
        case Elem(t,r) => loop( r, Elem( f(t), result ) ) 
      loop( this, Empty )
    }
      
    /*
     * Filtre une pile en enlevant les éléments qui ne réponde pas
     * à un prédicat
     */
    def filter( f: T=>Boolean ): Stack[T] = {
      @scala.annotation.tailrec
      def loop(rest: Stack[T], result: Stack[T] ): Stack[T] = rest match
        case Empty => result.reverse
        case Elem(t,r) if f(t) => loop( r, Elem( t, result ) ) 
        case Elem(_,r) => loop(r,result)
      loop( this, Empty )
    }

    /*
     * Réduit une pile à l'aide d'une fonction binaire. Si la liste est
     * vide, la valeur 'e' est retournée.
      */
    def reduce[U >: T]( e: U )( f: (U,U)=>U ): U = 
      @scala.annotation.tailrec
      def loop(rest: Stack[T], result: U ): U = rest match
        case Empty => result
        case Elem(t,r) => loop( r, f(result,t) ) 
      loop(this,e)
                            
    /*
     * Join deux piles en groupant les éléments par position.
     * si elles sont de longueur différente. Prendre la plus courte.
     */
    def zip[U]( that: Stack[U] ): Stack[(T,U)] = 
      def loop( rest1: Stack[T], rest2: Stack[U], result: Stack[(T,U)] ): Stack[(T,U)] =
        (rest1,rest2) match
          case (Empty,_) => result
          case (_,Empty) => result
          case (Elem(t,r1),Elem(u,r2)) => loop(r1,r2, Elem((t,u),result) )

      loop( this, that, Empty ).reverse
}

object Exemple {
  import Stack.*
  val v = Elem( 1, Elem( 2, Elem( 3, Empty ) ) )
  val u = Elem( -1, Elem(0, Elem(2, Empty ) ) )
  val w = v.zip(u).map( _ + _ )
  val a = v.zip(u).map( _ * _ ).reduce(0)(_+_)


}
