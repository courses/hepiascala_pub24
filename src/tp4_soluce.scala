package ch.hepia.tp

import java.time.LocalDate

case class Street(name: String, number: Int)
case class Address(
    street: Street,
    postCode: Int,
    place: String,
    country: String
)
case class Person(
    first: String,
    last: String,
    birth: LocalDate,
    address: Address
)

case class Point(x: Int, y: Int)
case class Segment(p1: Point, p2: Point)

enum Direction(a: Boolean, b: Boolean) {
  case Horizontal extends Direction(true, false)
  case Vertical extends Direction(false, true)
  case Diagonal extends Direction(true, true)
  case Other extends Direction(false, false)

  def toTuple: (Boolean, Boolean) = (this.a, this.b)
}

def orientation(s: Segment): (Boolean, Boolean) =
  import Direction.*

  val dir = s match
    case Segment(Point(x1, _), Point(x2, _)) if x1 == x2 => Vertical
    case Segment(Point(_, y1), Point(_, y2)) if y1 == y2 => Horizontal
    case Segment(Point(x1, y1), Point(x2, y2))
        // (x1 == y1 && x2 == y2) || (x1 == -y1 && x2 == -y2 )
        if abs(x2 - x1) == abs(y2 - y1) =>
      Diagonal
    case _ => Other

  dir.toTuple

case class Vec2(x: Double, y: Double) {
  def +(v: Vec2): Vec2 = plus(v)
  def plus(v: Vec2): Vec2 =
    Vec2(x + v.x, y + v.y)

  def *(k: Double): Vec2 = Vec2(k * x, k * y)

  def -(v: Vec2): Vec2 = this + (v * -1.0)

  def *(v: Vec2): Double = x * v.x + y * v.y

  def norm = math.sqrt(this * this)

}

@main
def tp4() = {
  val s = Street("Rue de la prairie", 4)
  val a = Address(s, 1202, "Geneva", "Switzerland")
  val michael = Person("Michael", "El Kharroubi", LocalDate.of(1994, 5, 13), a)
  println(michael)

  println(orientation(Segment(Point(0, 0), Point(0, 1))))
  println(orientation(Segment(Point(0, 0), Point(1, 0))))
  println(orientation(Segment(Point(0, 0), Point(1, 1))))
  println(orientation(Segment(Point(5, -5), Point(12, 17))))

  val v = Vec2(0, -1)
  val w = Vec2(1, 2)

  println(v.plus(w))
  println(v plus w)
  println(v.+(w))
  println(v + w)
}
