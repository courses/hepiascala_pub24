package ch.hepia.scala.tp7.soluce


/* Implémentez les fonctions suivantes. 
 */

case class Album( title: String, artist: String, year: Int )
case class Duration( minutes: Int, seconds: Int )
case class Track( title: String, duration: Duration )

val albums = List(
  Album( "Mit Gas", "Tomahawk", 2003 ),
  Album( "Pork Soda", "Primus", 1993 ),
  Album( "Brown Album", "Primus", 1997 ),
  Album( "Distraction Pieces", "Scroobius Pip", 2011 )
)

val tracks = Map(
  "Mit Gas" -> List(
    Track( "Mayday", Duration( 3, 32 ) )
  ),
  "Pork Soda" -> List(
    Track( "DMV", Duration( 4, 58 ) ),
    Track( "Mr. Krinkle", Duration( 5, 27 ) )
  ),
  "Brown Album" -> List(
    Track( "Fisticuffs", Duration( 4, 25 ) ),
    Track( "Camelback Cinema", Duration( 4, 0 ) ),
    Track( "Kalamazoo", Duration( 3, 31 ) )
  ),
  "Distraction Pieces" -> List(
    Track( "Let 'Em Come", Duration( 4, 25 ) ),
    Track( "Domestic Silence", Duration( 3, 58 ) )
  )
)

/* Retourne la liste de morceaux associés à un artiste */
def tracksOf_( artist: String ): List[Track] = albums.flatMap {
  case Album( title, a, _ ) => if( a == artist ) tracks(title)
  else Nil
}
def tracksOf( artist: String ): List[Track] =
  for {
    album <- albums if album.artist == artist
    track <- tracks(album.title)
  } yield track

/* Retourne la liste de tous les morceaux de moins de 4 minutes */
def shortTracks_ : List[Track] = tracks.values.toList.flatten.filter( _.duration.minutes < 4 )
def shortTracks: List[Track] =
  for {
    (_,ts) <- tracks.toList
    t <- ts if t.duration.minutes < 4
  } yield t

/* Retourne les titres des morceaux antérieurs à une année */
def titlesBefore_( year: Int ): List[String] =
    albums
      .filter( _.year < year ) //: List[Album]
      .map( _.title )          //: List[String]
      .flatMap( tracks )       //: List[Track]
      .map(_.title)            //: List[String]
def titlesBefore( year: Int ): List[String] =
  for {
    a <- albums if a.year < year
    t <- tracks(a.title)
  } yield t.title


/* Calcule la durée totale de tous les morceaux disponibles.
   REMARQUE: ont veut que les secondes soient inférieures à 60 mais les
   minutes peuvent dépasser ce total.
 */
def dur2sec( dur: Duration ): Int = dur.minutes*60 + dur.seconds
def sec2dur( sec: Int ): Duration = Duration( sec/60, sec % 60 )
def addDur( d1: Duration, d2: Duration ) = sec2dur( dur2sec(d1) + dur2sec(d2) )
def totalDuration_ : Duration =
  tracks.values.toList.flatten
    .foldLeft( Duration(0,0) ){ (d,t) => addDur(d,t.duration) }
def totalDuration : Duration = {
  val totalSec = tracks.values.toList.flatten.map( t => dur2sec(t.duration) ).sum
  sec2dur( totalSec )
}
