package ch.hepia.tp.soluce3

def swap( nums: (Int,Int)): (Int,Int) =
  ( nums._2, nums._1 )


def swap_( nums: (Int,Int)): (Int,Int) =
  val (a,b) = nums
  (b,a)


def swap__( nums: (Int,Int)): (Int,Int) =
  (nums(1),nums(0))

type Point = (Double,Double)

def distance( p: Point, q: Point ): Double =
  val (px,py) = p
  val (qx,qy) = q
  math.sqrt((px-qx)*(px-qx) + (py-qy)*(py-qy))

type DMS = (Int,Int,Double,String)
type CoordDMS = (DMS,DMS)
type CoordDD = (Double,Double)

def convert(coord: CoordDMS):CoordDD = 
  def DMS2DD( dms: DMS ): Double =
    val (d,m,s,_) = dms
    d + m.toDouble/60 + s/3600
  val (lat,long) = coord
  val lat_ = lat match
    case (_,_,_,"N") => DMS2DD(lat)
    case (_,_,_,"S") => -DMS2DD(lat)
  val long_ = long match 
    case (_,_,_,"E") => DMS2DD(long) 
    case (_,_,_,"W") => -DMS2DD(long) 
  (lat_,long_)

