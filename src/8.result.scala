package ch.hepia.tpscala.result

enum Result[+E,+A] {

  case Err[E]( value: E ) extends Result[E,Nothing]  
  case OK[A]( value: A ) extends Result[Nothing,A]

  def map[B]( f: A=>B ): Result[E,B] = this match 
    case Err(e) => Err(e)
    case OK(a) => OK(f(a))

  //F >: E, signifie "F est un supertype de E"
  def flatMap_[B, F >:E]( f: A=>Result[F,B] ): Result[F,B] = 
    this match
      case Err(e) => Err(e)
      case OK(a) => f(a)


  def flatMap[B, F >:E]( f: A=>Result[F,B] ): Result[F,B] = 
    fold( Err(_), f )

  //Retourne le résultat si OK
  //transforme l'erreur en une valeur valide sinon
  //B >: A signifie "B est un supertype de A
  //Une seul implémentation possible !
/*  def get[B >: A]( f: E => B ): B = this match
    case Err(e) => f(e)
    case OK(a) => a
*/
  def get[B >: A]( f: E=>B ): B =
    fold(f, identity)

  //Une seul implémentation possible !
  def fold[B]( f: E => B, g: A => B ): B = this match
    case Err(e) => f(e)
    case OK(a) => g(a)

    def toOption: Option[A] = fold( _ => None, Some(_) )

}
object Result {
  //Si il y a au moins une erreur dans la liste, le résultat est une erreur
  //Si il n'y a que des succès, liste les succès
  def sequence[E,A]( input: List[Result[E,A]] ): Result[E,List[A]] = 
    def loop( 
      rem: List[Result[E,A]],
      result: Result[E,List[A]]): Result[E,List[A]] = 
        rem match
          case Nil => result
          case resA :: rest => 
            var result2 = resA.flatMap{ a => 
             result.map( 
               listA => a :: listA 
             )
            }
            loop( rest, result2 )
    loop( input, OK( Nil ) )
}


case class Config( hostname: String, port: Int )
object Config {
  private def line2keyValue( line: String ): Result[String,(String,String)] = {
    val elements = line.split("=")
    if( elements.size != 2 ) Result.Err("Syntax error: " + line )
    else Result.OK( (elements(0), elements(1) ) )
  }

  private def lines2map( lines: List[String] ): Result[String,Map[String,String]] =
    Result.sequence( lines.map(line2keyValue) ).map( _.toMap )


  private def getKey( kvMap: Map[String,String], key: String ): Result[String,String] =
    println(">>>>> " + kvMap )
    kvMap.get(key) match 
      case None => Result.Err( s"Missing key $key")
      case Some(a) => Result.OK(a)

  private def string2int( str: String ): Result[String,Int] = 
    try {
      Result.OK(str.toInt)
    } catch {
      _ => Result.Err( s"Cannot convert string $str to int")
    }

  //Utilisez lines2map pour parvenir au résultat
  //Attention au clés manquantes
  def parse( lines: List[String] ): Result[String,Config] = 
    for {
      kvMap <- lines2map(lines)
      hostname <- getKey( kvMap, "hostname" )
      portStr <- getKey( kvMap, "port" )
      port <- string2int(portStr)
    } yield Config( hostname, port )

}
