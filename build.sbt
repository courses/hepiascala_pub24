val scala3Version = "3.3.1"

lazy val root = project
  .in(file("."))
  .settings(
    name := "hepiascala",
    version := "2.2.0",
    scalaVersion := scala3Version,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.18" % "test"
    ),
    Compile / scalaSource := baseDirectory.value / "src",
    Test / scalaSource := baseDirectory.value / "test-src"
  )
