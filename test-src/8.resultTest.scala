package ch.hepia.tpscala
package ennonce

import org.scalatest.funsuite.AnyFunSuite

import result._

/****
 * 
 * Dans SBT vous pouvez n'exécuter que ces tests:
 * 
 *  > testOnly ch.hepia.tpscala.Result8Suite
 * 
 */

class Result8Suite extends AnyFunSuite {

  import Result.{OK,Err}

  test( "map" ) {
    val hello: Result[String,String] = OK("hello")
    val notFound: Result[Int,String] = Err(404)

    assert( hello.map(_.size) == OK(5) )
    assert( notFound.map(_.toString) == notFound )

    assert( hello.map(identity) == hello )
    assert( notFound.map(identity) == notFound )

    assert( hello.map(_.size).map( _ > 2) ==
      hello.map( _.size > 2 ) )

    assert( notFound.map(_ * 2 ).map( _.toString) ==
      notFound.map( i => (i*2).toString  ) )
  }

  test( "flatMap" ) {
    type Res[A] = Result[String,A]
    val goodHost: Res[String] = OK("hello.com")
    val badHost: Res[String] = Err("NA")
    val goodPort: Res[Int] = OK(80)
    val badPort: Res[Int] = Err("undefined")

    assert(
      goodHost.flatMap{ host =>
        goodPort.map{ port =>
          Config(host,port)
        }
      } == OK( Config("hello.com",80) )
    )

    assert( 
      goodHost.flatMap{ host =>
        badPort.map{ port =>
          Config(host,port)
        }
      } == badPort
    )

    assert( 
      badHost.flatMap{ host =>
        goodPort.map{ port =>
          Config(host,port)
        }
      } == badHost
    )

    assert(
      badHost.flatMap{ host =>
        badPort.map{ port =>
          Config(host,port)
        }
      } == badHost
    )
  }

  test("get") {
    val found: Result[String, Long] = OK(665)
    val notFound: Result[String,Long] = Err("not found")

    assert( found.get( _ => -1 ) == 665 )
    assert( notFound.get( _ => -1 ) == -1 )

    def str2status( str: String ) = str match {
      case "not found" => 404
      case "conflict" => 409
      case "unauthorized" => 403
      case _ => 400
    }

    assert( found.map( _ => 200).get(str2status) == 200 )
    assert( notFound.map( _ => 200).get(str2status) == 404 )
  }


  test( "parse config" ) {
    val lines1 = List(
      "port=8888",
      "hostname=www.example.com",
    )

    val lines2 = List(
      "port: 8888",
      "hostname: www.example.com",
    )

    val lines3 = List(
      "port=8888",
    )

    val lines4 = List(
      "port=HTTP",
      "hostname=www.example.com",
    )


    val default = Config("localhost", 8080 )

    assert( Config.parse( lines1 ).get( _ => default )
      == Config("www.example.com",8888) )

    assert( Config.parse( lines2 ).get( _ => default )
      == default )

    assert( Config.parse( lines3 ).get( _ => default )
      == default )

    assert( Config.parse( lines4 ).get( _ => default )
      == default )


  }





}
